const express = require('express');
const router = express.Router();
const connectEnsureLogin = require('connect-ensure-login');
const db = require('./../configuration/connectionstring.js');


//Get all users
router.get('/',
    connectEnsureLogin.ensureLoggedIn('/login'),
    function (req, res) {
        db.query('SELECT * FROM persons', function (error, results, fields) {
            if (error) throw error;
            res.send(results);
        });
    });

//post a single user to db
router.post('/', function (req, res) {
    var values = [req.body.email, req.body.FirstName, req.body.Lastname];
    const query = 'INSERT INTO persons set Email=?, FirstName=?, LastName=?';
    db.query(query, values, function (error, result, fields) {
        if (error) throw error;
        res.send('User added to database');
    });
});

//Delete a single user from the database
router.delete('/', function (req, res) {
    const value = [req.body.id];
    const query = 'DELETE FROM persons WHERE id=?';
    db.query(query, value, function (error, result, field) {
        if (error) throw error;
        res.send('User deleted from database');
    });
});

///Update a single user from the database
router.put('/', function (req, res) {
    const values = [req.body.Email, req.body.FirstName, req.body.LastName, req.body.id];
    const query = 'UPDATE persons SET Email=?, FirstName=?, LastName=? WHERE id=?';
    db.query(query, values, function (error, result, field) {
        if (error) throw error;
        res.send('user successfully updated');
    })
})
module.exports = router;