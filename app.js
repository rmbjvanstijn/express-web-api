const express = require('express');
const expressSession = require('express-session');
const app = express();
const bodyParser = require('body-parser');
const helmet = require('helmet');
const passport = require('passport');
const localStrategy = require('passport-local').Strategy;
const db = require('./configuration/connectionstring.js');


// Configure view engine to render EJS templates.
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');

//Settings http headers
app.use(helmet())

//Configures new passport strategy with local strategy.
//This returns false when a error occured, results is not correct or password is not the same as in the database
passport.use(
    new localStrategy(
        function (username, password, cb) {
            db.query(`SELECT * FROM users WHERE username = "${username}"`, function (error, results, fields) {
                if (error) { return cb(err) }
                if (!results) { return cb(null, false) }
                if (results[0].password != password) { return cb(null, false) }
                return cb(null, results[0])
            })
        }
    )
);


//app.use(require('morgan')('combined'));
//app.use(require('cookie-parser')());
//Changes the body of a request into req.body
app.use(bodyParser.urlencoded({ extended: true }));
//This puts the session in the cookies after succesfull authentication 
app.use(expressSession({ secret: 'zBHW<G%bAvl^n^q@<9S+>hzoE{f4,k', resave: false, saveUninitialized: false }));

// Initialize Passport and restore authentication state, if any, from the session
app.use(passport.initialize());
app.use(passport.session());


passport.serializeUser(function (user, cb) {
    cb(null, user.id);
});

passport.deserializeUser(function (id, cb) {
    db.query(`SELECT * FROM Users Where id = "${id}"`, function (error, results, fields) {
        if (error) { return cb(error); }
        cb(null, results);
    })
});

//Test Route
app.get('/login', function (req, res) {
    res.render('login');
})

app.post('/login',
    passport.authenticate('local',
        {
            successRedirect: '/profile',
            failureRedirect: '/login'
        }),
    function (req, res) {
        res.redirect('/');
    });

app.get('/logout',
    function (req, res) {
        req.logout();
        res.redirect('/');
    });

app.get('/profile',
    require('connect-ensure-login').ensureLoggedIn('/login'),
    function (req, res) {
        res.render('profile', { user: req.user });
    });

//Routes
const indexRouter = require('./routes/index');
const userRouter = require('./routes/users');

app.use('/', indexRouter);
app.use('/api/users', userRouter);

app.listen(3000, () => console.log('Listening on port 3000!'));

//Login with passport
//https://github.com/passport/express-4.x-local-example/blob/master/server.js#L85